https://github.com/pdoc3/pdoc
https://github.com/sanic-org/sanic -- fast webserver alt to flask
https://www.nexedi.com/NXD-Blog.Multicore.Python.HTTP.Server -- another fast python webserver alt to flask
https://sanic.dev/en/guide/advanced/websockets.html
https://sanic.dev/en/guide/basics/middleware.html
https://gitlab.com/pgjones/hypercorn
https://www.reddit.com/r/kivy/comments/lpxema/comment/goipzyv/

pip3 install sanic


below failed for me:

Sanic makes use of uvloop and ujson to help with performance. If you do not want to use those packages, simply add an environmental variable SANIC_NO_UVLOOP=true or SANIC_NO_UJSON=true at install time.

$ export SANIC_NO_UVLOOP=true
$ export SANIC_NO_UJSON=true
$ pip3 install --no-binary :all: sanic

