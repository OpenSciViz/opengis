from kivymd.app import MDApp

from kivy.uix.screenmanager import ScreenManager, Screen

from kivymd.uix.list import OneLineListItem, MDList

from kivy.uix.widget import Widget

from kivy.lang import Builder

from kivy.uix.boxlayout import BoxLayout

from kivymd.uix.dialog import MDDialog

from kivymd.uix.button import MDFlatButton

import json

from ws_client import WebSocketClient

import websockets

import asyncio



class ContentHomeScreen(BoxLayout):

pass



class MessagePanel(Screen):

dialog = None

def on_enter(self, *args):

self.openPopup()

# listsView = MDList()

# for Item in range(4):

# TwoLineLists = OneLineListItem(text='Single')

# listsView.add_widget(TwoLineLists)



# self.ids.listsHere.add_widget(listsView)



def openPopup(self):

self.dialog = MDDialog(title="Enter email address", type="custom", content_cls=ContentHomeScreen(), auto_dismiss=False, size_hint=(0.8, 0.5),

buttons=[

MDFlatButton(

text="CANCEL", on_release=self.closePopup

),

MDFlatButton(

text="CONNECT", on_release=self.connectToWebsocket

),

]

)

self.dialog.open()



def closePopup(self, Widget):

self.dialog.dismiss()



class WindowManager(ScreenManager):

pass



sm = WindowManager()



class MessageApp(MDApp):



def build(self):



screens = [

MessagePanel(name='messagePanel'),

]



for screen in screens:

sm.add_widget(screen)



sm.current = 'messagePanel'

return sm



if __name__ == '__main__':

MessageApp().run()
