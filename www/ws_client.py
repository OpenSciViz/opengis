here is the ws_client.py file which i want to use in my kivy app

import websockets

import asyncio

import json



class WebSocketClient():



def __init__(self):

pass



async def connect(self, email):

'''

Connecting to webSocket server



websockets.client.connect returns a WebSocketClientProtocol, which is used to send and receive messages

'''

self.connection = await websockets.client.connect('wss://7ex1bd32pi.execute-api.us-east-2.amazonaws.com/dev?userid='+email)

if self.connection.open:

print('Connection stablished. Client correcly connected')

# Send greeting

await self.sendMessage('Hey server, this is webSocket client')

return self.connection





async def sendMessage(self, message, ReceiverID):

'''

Sending message to webSocket server

'''

await self.connection.send(json.dumps({"Msg": message, "action": "onMessage", "ReceiverID": "any@app.com"}))



async def receiveMessage(self, connection):

'''

Receiving all server messages and handling them

'''

while True:

try:

message = await connection.recv()

print('Received message from server: ' + str(message))

except websockets.exceptions.ConnectionClosed:

print('Connection with server closed')

break



if __name__ == '__main__':

# Creating client object

client = WebSocketClient()

loop = asyncio.get_event_loop()

# Start connection and get client connection protocol

connection = loop.run_until_complete(client.connect())

# Start listener and heartbeat

tasks = [

asyncio.ensure_future(client.receiveMessage(connection)),

]



loop.run_until_complete(asyncio.wait(tasks))
