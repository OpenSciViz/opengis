#!/bin/bash
echo https://github.com/containers/podman/releases -- nov 10, 2022 podman 4.3.1 static binary 
echo https://stackoverflow.com/questions/70971713/mounting-volumes-between-host-macos-bigsur-and-podman-vm
echo above recommends '"podman machine init --now --cpus=4 --memory=4096 -v $HOME:$HOME"'
echo podman machine init --volume /opt --volume /opt/solrdata
# first attempt failed due to voloume mystery
#podman run -d -v "/opt/solrdata:/var/solr" -p 8983:8983 --name hon_solr solr solr-precreate gettingstarted
# podman machine ssh and snooping aroung file sysytem suggested this to me:
echo 'podman run -d -v "/opt/solrdata:/var/opt/solrdata" -p 8983:8983 --name hon_solr solr solr-precreate gettingstarted'
echo '... if above runs peoperly, try post cli, which runs in container bash? so $docdir must exist within container as host voulme mount):'
host_docdir=/opt/solrdata
contnr_docdir=/var/opt/solrdata # ???
echo podman exec -it hon_solr post -c gettingstarted $docdir
docs=('/var/opt/solrdata/2021.10.20_10.50_NOAA_NESDIS Cloud Archive\ Pilot\ \(NCAP\).pdf' '/var/opt/solrdata/Algorithm_Theoretical_Basis_Document_Template_2.2.docx.pdf' '/var/opt/solrdata/CalPOPcharter_Dec2009.doc.pdf' '/var/opt/solrdata/Ccode_v1.0\ \(1\).docx.pdf' '/var/opt/solrdata/Charter_IDBOP_v1.docx.pdf' '/var/opt/solrdata/esd-fleet-swoosh-120122.pdf' '/var/opt/solrdata/External_Users_Manual_Template_2.3.docx.pdf' '/var/opt/solrdata/fortran77_v2.1.docx.pdf' '/var/opt/solrdata/fortran95_v2.1.docx.pdf' '/var/opt/solrdata/FY08_Metrics_FINAL.doc.pdf' '/var/opt/solrdata/FY09_Metrics_PRELIM.doc.pdf' '/var/opt/solrdata/FY\ 2023\ NESDIS Omnibus Summary.pdf' '/var/opt/solrdata/general_standards_v2.0.docx.pdf' '/var/opt/solrdata/Global_Hydro_Estimator_RAD_v1.0_090911.doc.pdf' '/var/opt/solrdata/ICAPOP_charter.doc.pdf' '/var/opt/solrdata/Internal_Users_Manual_Template_2.2.docx.pdf' '/var/opt/solrdata/LSPOP_CHARTER.doc.pdf' '/var/opt/solrdata/N4RT_RAD_v1.3.docx.pdf' '/var/opt/solrdata/NCCF PI-10 Planning - January 2023.pptx.pdf' '/var/opt/solrdata/NESDIS Organization Chart Summer 2023 .docx.pdf' '/var/opt/solrdata/OCPOP_CHARTER.doc.pdf' '/var/opt/solrdata/OPOP_charter.doc.pdf' '/var/opt/solrdata/Satellite\ Products\ and\ Services\ Review\ Board\ Site\ Migration.pdf' '/var/opt/solrdata/SME\ for\ Cloud\ Development\ and\ Innovation\ Initiatives\ Training.docx.pdf' '/var/opt/solrdata/SOP_Charter.doc.pdf' '/var/opt/solrdata/SOUPOP_charter.doc.pdf' '/var/opt/solrdata/SPSRB_Doc_Template.doc.pdf' '/var/opt/solrdata/SPSRB_Documents_Guidelines_2.0.docx.pdf' '/var/opt/solrdata/SPSRB_Process_Paper_Ver_13.docx.pdf' '/var/opt/solrdata/System_Maintenance_Manual_Template_2.3.docx.pdf' '/var/opt/solrdata/U10489057_20221201_20221215.pdf' '/var/opt/solrdata/Vol\ 1 3.6.\ CRH\ CAC\ Policy.pdf' '/var/opt/solrdata/WPOP_CHARTER.doc.pdf')
echo ${docs[@]}
echo podman exec -it hon_solr post -c gettingstarted $contnr_docdir
#for doc in ${docs[@]}; do
#  echo podman exec -it hon_solr post -c gettingstarted $doc
#done
