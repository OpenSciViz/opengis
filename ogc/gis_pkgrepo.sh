#from https://docs.qgis.org/2.14/en/docs/user_manual/working_with_ogc/ogc_server_support.html
cat >> /etc/apt/sources.list.d/debian-gis.list
deb http://qgis.org/debian trusty main
deb-src http://qgis.org/debian trusty main

# Add keys
sudo gpg --keyserver keyserver.ubuntu.com --recv-key 3FF5FFCAD71472C4
sudo gpg --export --armor 3FF5FFCAD71472C4 | sudo apt-key add -

# Update package list
sudo apt-get update && sudo apt-get upgrade
sudo apt-get install qgis-server python-qgis

sudo mkdir -p /opt/qgis-server/plugins
cd /opt/qgis-server/plugins
sudo wget https://github.com/elpaso/qgis-helloserver/archive/master.zip
# In case unzip was not installed before:
sudo apt-get install unzip
sudo unzip master.zip
sudo mv qgis-helloserver-master HelloServer

