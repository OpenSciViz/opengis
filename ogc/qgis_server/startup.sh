#!/bin/bash
echo https://docs.qgis.org/3.22/en/docs/server_manual/containerized_deployment.html
echo try running this as either root or priviledged user
id
echo docker network ls
echo docker images
echo docker network create qgis
echo docker network ls
echo docker pull nginx
echo docker build -f Dockerfile -t qgis-server ./
echo docker images
echo docker ps -a
echo docker run -d --name qgis-server --net=qgis --hostname=qgis-server -v $(pwd)/data:/data:ro -p 5555:5555 -e "QGIS_PROJECT_FILE=/data/osm.qgs" qgis-server
echo docker run -d --rm --name nginx --net=qgis --hostname=nginx -v $(pwd)/nginx.conf:/etc/nginx/conf.d/default.conf:ro -p 8080:80 nginx
echo docker ps -a
echo docker exec -it nginx bash
echo docker exec -it qgis-server bash
echo docker exec -itu 0 qgis-server bash
echo 'docker logs qgis-server &> qgis-serverl.log'
echo 'docker logs nginx &> nginx.log'
