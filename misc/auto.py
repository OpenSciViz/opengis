The Repetitive task is always time-consuming and boring to do. Think about work like you want to Crop 100 Photos one by one or Fetch API, Correcting Spells and Grammer and etc. All these tasks are time-consuming so why not automate them? In this article, I will show you 10 Python Automation Scripts. So put this article on your list and let's get started.

In the Life of Software Developer, Learning Never Ends…

— Albert Sidhant

👉 Image Optimizer
Make your images better or edit them as you did in Photoshop with this awesome automation script. This script uses the popular Pillow module, below you can find the most of methods that you will need to optimize your images.

Use in your Image Editing Project
Use this in your Python Projects
Bulk Image Editing
Much more
# Image Optimizing
# pip install Pillow
import PIL
# Croping 
im = PIL.Image.open("Image1.jpg")
im = im.crop((34, 23, 100, 100))
# Resizing
im = PIL.Image.open("Image1.jpg")
im = im.resize((50, 50))
# Flipping
im = PIL.Image.open("Image1.jpg")
im = im.transpose(PIL.Image.FLIP_LEFT_RIGHT)
# Rotating
im = PIL.Image.open("Image1.jpg")
im = im.rotate(360)
# Compressing
im = PIL.Image.open("Image1.jpg")
im.save("Image1.jpg", optimize=True, quality=90)
# Bluring
im = PIL.Image.open("Image1.jpg")
im = im.filter(PIL.ImageFilter.BLUR)
# Sharpening
im = PIL.Image.open("Image1.jpg")
im = im.filter(PIL.ImageFilter.SHARPEN)
# Set Brightness
im = PIL.Image.open("Image1.jpg")
im = PIL.ImageEnhance.Brightness(im)
im = im.enhance(1.5)
# Set Contrast
im = PIL.Image.open("Image1.jpg")
im = PIL.ImageEnhance.Contrast(im)
im = im.enhance(1.5)
# Adding Filters
im = PIL.Image.open("Image1.jpg")
im = PIL.ImageOps.grayscale(im)
im = PIL.ImageOps.invert(im)
im = PIL.ImageOps.posterize(im, 4)
# Saving
im.save("Image1.jpg")
👉 Video Optimizer
Not only Images you can use Python to optimize videos too by using the below automation script. This script uses the Moviepy module that allows you to Trim, add audio, set the speed of the video, add VFX, and much more.

Create a Full Video Editor
Use in your Python Projects
Trimming Videos
Make Video from Images
Much more
# Video Optimizer
# pip install moviepy
import moviepy.editor as pyedit
# Load the Video
video = pyedit.VideoFileClip("vid.mp4")
# Trimming
vid1 = video.subclip(0, 10)
vid2 = video.subclip(20, 40)
final_vid = pyedit.concatenate_videoclips([vid1, vid2])
# Speed up the video
final_vid = final_vid.speedx(2)
# Adding Audio to the video
aud = pyedit.AudioFileClip("bg.mp3")
final_vid = final_vid.set_audio(aud)
# Reverse the Video
final_vid = final_vid.fx(pyedit.vfx.time_mirror)
# Merge two videos
vid1 = pyedit.VideoFileClip("vid1.mp4")
vid2 = pyedit.VideoFileClip("vid2.mp4")
final_vid = pyedit.concatenate_videoclips([vid1, vid2])
# Add VFX to Video
vid1 = final_vid.fx(pyedit.vfx.mirror_x)
vid2 = final_vid.fx(pyedit.vfx.invert_colors)
final_vid = pyedit.concatenate_videoclips([vid1, vid2])
# Add Images to Video
img1 = pyedit.ImageClip("img1.jpg")
img2 = pyedit.ImageClip("img2.jpg")
final_vid = pyedit.concatenate_videoclips([img1, img2])
# Save the video
final_vid.write_videofile("final.mp4")
👉 PDF to Images
This small automation script can conveniently fetch the entire PDF pages and convert them to Images. This script uses the popular PyMuPDF module that is well known for its PDF Text extraction.

Use this in your PDF Projects
Bulk PDF to Images
Much more
# PDF to Images
# pip install PyMuPDF
import fitz
def pdf_to_images(pdf_file):
    doc = fitz.open(pdf_file)
    for p in doc:
        pix = p.get_pixmap()
        output = f"page{p.number}.png"
        pix.writePNG(output)
pdf_to_images("test.pdf")
👉 Fetch API Data
Need to fetch API data from a database or need to send API requests to servers. Then this automation script is a handy tool for you. It uses the Urllib3 module that will let you Fetch and Post API requests.

# pip install urllib3
import urllib3
# Fetch API data
url = "https://api.github.com/users/psf/repos"
http = urllib3.PoolManager()
response = http.request('GET', url)
print(response.status)
print(response.data)
# Post API data
url = "https://httpbin.org/post"
http = urllib3.PoolManager()
response = http.request('POST', url, fields={'hello': 'world'})
print(response.status)
👉 Battery Indicator
This handy script will let you set your percentage of battery on that you want to get notified. This script uses Pyler for notification and Psutil to get the current percentage of the battery.

# Battery Notifier
# pip instal plyer
from plyer import notification
import psutil
from time import sleep
while True:
    battery = psutil.sensors_battery()
    life = battery.percent
    if life < 50:
        notification.notify(
            title = "Battery Low",
            message = "Please connect to power source",
            timeout = 10
        )
    sleep(60)
👉 Grammer Fixer
Tired of Proofreading your Long articles or Text then try this automation script that will scan your Text and correct the Grammatical Errors. This awesome script uses Happtransformer Module which is a Machine learning module that is trained to fix Grammer Errors in Text.

# Grammer Fixer
# pip install happytransformer
from happytransformer import HappyTextToText as HappyTTT
from happytransformer import TTSettings
def Grammer_Fixer(Text):
    Grammer = HappyTTT("T5","prithivida/grammar_error_correcter_v1")
    config = TTSettings(do_sample=True, top_k=10, max_length=100)
    corrected = Grammer.generate_text(Text, args=config)
    print("Corrected Text: ", corrected.text)
Text = "This is smple tet we how know this"
Grammer_Fixer(Text)
👉 Spelling Fixer
This awesome script will help you correct your Text words spell. Below you can find the script that will you how you can fix single words or multiple words in a sentence.

# Spell Fixer
# pip install textblob
from textblob import *
# Fixing Paragraph Spells
def fix_paragraph_words(paragraph):
    sentence = TextBlob(paragraph)
    correction = sentence.correct()
    print(correction)
# Fixing Words Spells
def fix_word_spell(word):
    word = Word(word)
    correction = word.correct()
    print(correction)
fix_paragraph_words("This is sammple tet!!")
fix_word_spell("maangoo")
👉 Internet Downloader
You guys probably use the Downloading software for downloading photos or videos from the internet but now you can create your own downloader by using the Python IDM module.

Download Google Photos
Use in your Project
Download Videos and Music
Much more
# Python Downloader
# pip install internetdownloadmanager
import internetdownloadmanager as idm
def Downloader(url, output):
    pydownloader = idm.Downloader(worker=20,
                                part_size=1024*1024*10,
                                resumable=True,)
    
    pydownloader .download(url, output)
Downloader("Link url", "image.jpg")
Downloader("Link url", "video.mp4")
👉 Fetch World News
Keep yourself updated with daily World news by using this automation script that will let you fetch news from any country with any language. This API let you fetch 50 News articles daily for Free.

# World News Fetcher
# pip install requests
import requests
ApiKey = "YOUR_API_KEY"
url = "https://api.worldnewsapi.com/search-news?text=hurricane&api-key={ApiKey}"
headers = {
  'Accept': 'application/json'
}
response = requests.get(url, headers=headers)
print("News: ", response.json())
👉 PySide2 GUI
This automation script will help you create your GUI application with the PySide2 Gui module. Below you can find every method that you need to start developing a decent modern App.

PySide2 also supports cross-platform and is very user-friendly for developers check out the code below.

# PySide 2 
# pip install PySide2
from PySide6.QtWidgets import *
from PySide6.QtGui import *
import sys
app = QApplication(sys.argv)
window = QWidget()
# Resize the Window
window.resize(500, 500)
# Set the Window Title
window.setWindowTitle("PySide2 Window")
# Add Buttons
button = QPushButton("Click Me", window)
button.move(200, 200)
# Add Label Text
label = QLabel("Hello Medium", window)
label.move(200, 150)
# Add Input Box
input_box = QLineEdit(window)
input_box.move(200, 250)
print(input_box.text())
# Add Radio Buttons
radio_button = QRadioButton("Radio Button", window)
radio_button.move(200, 300)
# Add Checkbox
checkbox = QCheckBox("Checkbox", window)
checkbox.move(200, 350)
# Add Slider
slider = QSlider(window)
slider.move(200, 400)
# Add Progress Bar
progress_bar = QProgressBar(window)
progress_bar.move(200, 450)
# Add Image 
image = QLabel(window)
image.setPixmap(QPixmap("image.png"))
# Add Message Box
msg = QMessageBox(window)
msg.setText("Message Box")
msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
window.show()
sys.exit(app.exec()


