# OpenGIS

Open source GIS
Leaflet and Openlayers and WebGL
https://github.com/qgis/QGIS.git


Java:
https://geoserver.org/release/stable/

C++:

https://github.com/usgs/pestpp.git -- USGS PEST++ is a software suite aimed at supporting complex numerical models in the decision-support context. Much focus has been devoted to supporting environmental models (groundwater, surface water, etc) but these tools are readily applicable to any computer model.`

Python:

https://github.com/usgs/gspy -- USGS repos of interest. TBD: does USGS use CMR?

https://github.com/usgs/shakemap.git

pip3 install kivy mapview kivy-garden.mapviewa -- potential mobile platform

https://realpython.com/mobile-app-kivy-python/

https://mapview.readthedocs.io/en/latest/usage/

https://github.com/flatplanet/kivyMD/blob/main/map.py

https://github.com/bitcraft/pytmx

https://github.com/geopython -- OGC WPS and more

https://github.com/IAMconsortium/pyam

Mapbox:
https://github.com/mapbox/mapbox-gl-js -- javascript

https://github.com/mapbox/mapbox-gl-native-android -- c++ or ?

https://github.com/mapbox/mapbox-maps-ios.git -- swiftlang


