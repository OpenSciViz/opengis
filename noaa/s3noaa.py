#!/usr/bon/env python

def himawari9(hurl='https://noaa-himawari9.s3.amazonaws.com/index.html#AHI-L1b-Target/2023/01/01/'):
  """
  The JAXA Himawari9 sat datasets shall be ingested into NESDIS alongside all GOEAS data.
  """
  url_himawari9 = 'https://noaa-himawari9.s3.amazonaws.com/index.html#AHI-L1b-Target/2023/01/01/{0000,0010,0020,...0810}'
  if not(hurl): hurl = url_himawari9
  base_url = []
  for item in range(10,820,10):
    sitem = str(item)
    url = hurl + '00' + sitem
    if(item > 100):
      url = hurl + '0' + sitem
    if(item > 1000):
      url = hurl + sitem

    url += '/' ; # print(url)
    base_url.append(url)
    ucnt = len(base_url) ; print("url cnt:", ucnt)
  return base_url

def main(args):
  """
  Himawari data indexing CLI script args parser and associated func entry.
  All metadata shall be placed into a catalogue server and both metadata and raw data into AWS S3 buckets
  """
  hurls = himawari9()
  print(hurls)

if __name__ == "__main__":
  args = []
  if len(sys.argv) > 1: args = copy.deepcopy(sys.argv)
  res = main(args)
  sys.exit(0)

