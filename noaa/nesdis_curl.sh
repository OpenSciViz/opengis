#!/bin/bash
^#curl -v -XPOST -H "Content-type: application/json" -H "Echo-token: 0000000000" internal-cmr-services-prod-577659398.us-east-1.elb.amazonaws.com/ingest/providers -d '{"provider-id": "NCEI", "short-name": "NCEI", "cmr-only": true, "small": false}'
^#curl -i -XPUT -H "Content-type: application/iso19115+xml" -H "Echo-Token: 0000000000" http://internal-cmr-services-prod-577659398.us-east-1.elb.amazonaws.com/ingest/providers/NCEI/collections/test1 -d @C1597928934-NOAA_NCEI.native
^#curl -i -XPUT -H "Content-type: application/vnd.nasa.cmr.umm+json;version=1.15" -H "Echo-Token: 0000000000" http://internal-cmr-services-prod-577659398.us-east-1.elb.amazonaws.com/ingest/providers/NCEI/collections/test1 -d @noaa-c.json
 curl -i -XPUT -H "Content-type: application/vnd.nasa.cmr.umm+json;version=1.6.1" -H "Echo-Token: 0000000000" http://internal-cmr-services-prod-577659398.us-east-1.elb.amazonaws.com/ingest/providers/NCEI/granules/testgranule1 -d @noaa-g.json
^#curl -i -XPUT -H "Content-type: application/vnd.nasa.cmr.umm+json;version=1.6.1" -H "Echo-Token: 0000000000" http://internal-cmr-services-prod-577659398.us-east-1.elb.amazonaws.com/ingest/providers/NCEI/granules/testgranule2 -d @noaa-g-2.json
^#curl -H "Echo-token: 0000000000"  http://internal-cmr-services-prod-577659398.us-east-1.elb.amazonaws.com/search/collections.json
^#curl -H "Echo-Token: 0000000000" http://internal-cmr-services-prod-577659398.us-east-1.elb.amazonaws.com/search/granules.json?provider=NCEI
^#curl -v -XPOST -H "Content-type: application/json" -H "Echo-token: 0000000000" https://ncis-cmr.nesdis-hq.noaa.gov/ingest/providers -d '{"provider-id": "NCEI", "short-name": "NCEI", "cmr-only": true, "small": false}'
^#curl -i -XPUT -H "Content-type: application/iso19115+xml" -H "Echo-Token: 0000000000" https://ncis-cmr.nesdis-hq.noaa.gov/ingest/providers/NCEI/collections/test1 -d @C1597928934-NOAA_NCEI.native
^#curl -i -XPUT -H "Content-type: application/vnd.nasa.cmr.umm+json;version=1.15" -H "Echo-Token: 0000000000" https://ncis-cmr.nesdis-hq.noaa.gov/ingest/providers/NCEI/collections/test1 -d @noaa-c.json
^#curl -i -XPUT -k -H "Content-type: application/iso19115+xml" -H "Echo-Token: 0000000000" https://ncis-cmr.nesdis-hq.noaa.gov/ingest/providers/TEST0/collections/HazardPhotos_August_1959 -d @Hazards_RESOLVED.xml
 curl -i -XPUT -H "Content-type: application/vnd.nasa.cmr.umm+json;version=1.6.1" -H "Echo-Token: 0000000000" https://ncis-cmr.nesdis-hq.noaa.gov/ingest/providers/NCEI/granules/testgranule1 -d @noaa-g.json
^#curl -i -XPUT -H "Content-type: application/vnd.nasa.cmr.umm+json;version=1.6.1" -H "Echo-Token: 0000000000" https://ncis-cmr.nesdis-hq.noaa.gov/ingest/providers/NCEI/granules/testgranule2 -d @noaa-g-2.json
^#curl -i -k -XPUT -H "Content-type: application/echo10+xml" -H "Echo-Token: 0000000000" https://ncis-cmr.nesdis-hq.noaa.gov/ingest/providers/TEST0/granules/HazardPhotos_ATL05 -d @G181127451-ASF_Sample.xml
^#curl -i -XPOST -k -H "Content-type: application/iso19115+xml" -H "Echo-Token: 0000000000" https://ncis-cmr.nesdis-hq.noaa.gov/ingest/providers/NCEI/validate/collection/sample_name -d @OISST_RECORD.xml
^#curl -i -XPOST -H "Content-type: application/iso:smap+xml" -H "Echo-Token: 0000000000"  https://ncis-cmr.nesdis-hq.noaa.gov/ingest/providers/TEST0/granule/HazardPhotos_August_1959 -d @granule.xml
^#curl -i -XDELETE -H “Echo-Token: 0000000000” ‘https://ncis-cmr.nesdis-hq.noaa.gov/ingest/providers/provider-id/granules/granule-nativeId’
^#curl -k -H "Echo-token: 0000000000"  https://ncis-cmr.nesdis-hq.noaa.gov/search/collections.json |jq
^#curl -k -H "Echo-Token: 0000000000" https://ncis-cmr.nesdis-hq.noaa.gov/search/granules.json?provider=NCEI |jq
^#curl -k -XPOST -i -H "Content-Type: application/json" -H "Echo-Token: 0000000000" https://ncis-cmr.nesdis-hq.noaa.gov/access-control/acls -d '{
^#curl -XPOST -H"Content-Type: application/json" -H "Echo-Token: 0000000000" http://internal-cmr-services-prod-577659398.us-east-1.elb.amazonaws.com/indexer/reindex-provider-collections -d '["NCEI"]'
^#curl -XPOST -H"Content-Type: application/json" -H "Echo-Token: 0000000000" http://internal-cmr-services-uat-1206488600.us-east-1.elb.amazonaws.com/indexer/reindex-provider-collections -d '["NCEI"]'
^#curl -H "Echo-token: 0000000000"  http://internal-cmr-services-uat-1206488600.us-east-1.elb.amazonaws.com
^#curl -H "Echo-Token: 0000000000" http://internal-cmr-services-uat-1206488600.us-east-1.elb.amazonaws.com/search/granules.json?provider=NCEI
^#curl -XPOST -H"Content-Type: application/json" -H "Echo-Token: 0000000000" http://internal-cmr-services-prod-577659398.us-east-1.elb.amazonaws.com/ingest/jobs/reindex-provider-collections
^#curl -i -XPOST -H"Content-Type: application/json" -H "Echo-Token: 0000000000" http://internal-cmr-services-prod-577659398.us-east-1.elb.amazonaws.com/ingest/jobs/reindex-all-collections?force_version=true
^#curl -i -XPOST -H"Authorization: <token>" http://<lb>/bootstrap/bulk_index/providers/all
^#curl -XPOST -k -H"Echo-Token: 0000000000" https://ncis-cmr.nesdis-hq.noaa.gov/access-control/reindex-acls
^#curl -XPOST -k -H"Echo-Token: 0000000000" https://ncis-cmr.nesdis-hq.noaa.gov/ingest/jobs/reindex-collection-permitted-groups
^#curl -XPOST -H"Content-Type: application/json" -H "Echo-Token: 0000000000" https://ncis-cmr.nesdis-hq.noaa.gov/indexer/reindex-provider-collections -d '["AO"]'
^#curl -X POST -H "Content-Type: application/json" "internal-cmr-es-pr-ElasticC-YDPZON6KVK8B-291568446.us-east-1.elb.amazonaws.com:9200/_snapshot/cmr_s3_repository/snapshot-05-19-2022/_restore" -d '{ "include_global_state": "true" }'
^#curl -k -XPOST -H"Authorization: 0000000000" https://ncis-cmr.nesdis-hq.noaa.gov/access-control/db-migrate
^#curl -k -XPOST -H"Authorization: 0000000000" https://ncis-cmr.nesdis-hq.noaa.gov/bootstrap/db-migrate
^#curl -k -XPOST -H"Authorization: 0000000000" https://ncis-cmr.nesdis-hq.noaa.gov/indexer/db-migrate
^#curl -k -XPOST -H"Authorization: 0000000000" https://ncis-cmr.nesdis-hq.noaa.gov/ingest/db-migrate
^#curl -k -XPOST -H"Authorization: 0000000000" https://ncis-cmr.nesdis-hq.noaa.gov/metadata-db/db-migrate
^#curl -k -XPOST -H"Content-Type: application/json" -H "Echo-Token: 0000000000" https://ncis-cmr.nesdis-hq.noaa.gov/indexer/reindex-provider-collections -d '["NCEI"]'
