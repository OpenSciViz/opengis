#!/bin/bash
echo https://awscli.amazonaws.com/v2/documentation/api/latest/reference/s3/sync.html#examples
echo ''
echo 'some aws s3 bucket cli samples -- mv file ... create new bucket ... reorg buckets ...'
echo ''
echo 'aws s3 mv s3://bucket/folder/file1 s3://bucket/folder/file2'
echo 'aws s3 mb s3://new-bucket'
echo 'aws s3 mv s3://old-bucket s3://new-bucket --recursive'
echo 'aws s3 rb s3://old-bucket'
echo ''
echo '############################### python aws s3 #######################'
echo ''

cat <<PYTEXT
#!/usr/bin/env python3
import s3fs
path1='s3:///bucket_name/folder1/sample_file.pkl'
path2='s3:///bucket_name/folder2/sample_file.pkl'

credentials= {}
credentials.setdefault("region_name", r_name) # mention the region
credentials.setdefault("aws_access_key_id", a_key) # mention the access_key_id
credentials.setdefault("aws_secret_access_key", s_a_key) # mention the 
secret_access_key

s3=s3fs.S3FileSystem(client_kwargs=credentials)

s3.move(path1,path2)

PYTEXT

