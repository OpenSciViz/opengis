#!/usr/bin/env python
import sys
#from kivy_garden.mapview import MapView
from kivy.garden.mapview import MapView, MapMarker
from kivy.app import App

_zoom = 11
_lat = 50.6394
_lon = 3.057
_mapurl = 'https://tile.openstreetmap.org/{z}/{x}/{y}.png'

class MapViewApp(App):
  def build(self):
    map = MapView(zoom = _zoom, lat = _lat, lon = _lon)
    m1 = MapMarker(lon=50.6394, lat=3.057)  # Lille
    map.add_marker(m1)
    m2 = MapMarker(lon=-33.867, lat=151.206)  # Sydney
    map.add_marker(m2)
    return map

def main(args):
  """
  pip3 install kivy[full] kivy-garden 
  garden install matplotlib
  garden install grapgh
  https://realpython.com/mobile-app-kivy-python/
  https://mapview.readthedocs.io/en/latest/usage/
  https://github.com/flatplanet/kivyMD/blob/main/map.py
  """
  map = MapViewApp()
  source = MapSource(url=_mapurl,
                     cache_key="OSMtiles", tile_size=512,
                     image_ext="png", attribution="@ NESDIS")
  map.map_source = source
  map.run()

if __name__ == "__main__":
  args = []
  if len(sys.argv) > 1: args = copy.deepcopy(sys.argv)
  res = main(args)
  sys.exit(0)

