#!/usr/bin/env python
from kivy_garden.mapview import MapView
from kivy.app import App

class MapViewApp(App):
  """
  pip3 install kivy[full]
  python -m pip install kivy_garden.mapview --extra-index-url https://kivy-garden.github.io/simple/
  """
  def build(self):
    mapview = MapView(zoom=11, lat=50.6394, lon=3.057)
    return mapview

MapViewApp().run()


