#!/bin/bash
echo sudo apt install python-is-python3

# from: https://kivy.org/doc/stable-1.11.0/installation/installation-linux-venvs.html
# Make sure Pip, Virtualenv and Setuptools are updated
echo sudo pip install --upgrade pip virtualenv setuptools

# create a virtualenv named "kivy_venv  or whatev
echo virtualenv --no-site-packages kivy_venv
echo source ./kivy_venv
echo pip3 install kivy[full]
echo python3 -m pip install kivy_garden.mapview --extra-index-url https://kivy-garden.github.io/simple/

#=============================================
# from https://pypi.org/project/kivy-garden.mapview/ 
#
#pip install mapview
#from kivy_garden.mapview import MapView
#from kivy.app import App
#
#class MapViewApp(App):
#  def build(self):
#    mapview = MapView(zoom=11, lat=50.6394, lon=3.057)
#    return mapview
#
#MapViewApp().run()
#

#==========================================

#Refs:
#https://github.com/kivy-garden/mapview.git
#https://pypi.org/project/kivy-garden.mapview/
#https://kivy.org/doc/stable/guide/packaging-android.html
#https://github.com/kivy/python-for-android

