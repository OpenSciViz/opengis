#!/bin/bash
echo sudo apt install python-is-python3

# from: https://kivy.org/doc/stable-1.11.0/installation/installation-linux-venvs.html
# Make sure Pip, Virtualenv and Setuptools are updated
echo sudo pip install --upgrade pip virtualenv setuptools

# create a virtualenv named "kivy_venv  or whatev
echo virtualenv --no-site-packages kivy_venv
echo source ./kivy_venv
echo pip3 install kivy[full]
echo python3 -m pip install kivy_garden.mapview --extra-index-url https://kivy-garden.github.io/simple/


#=============================================
# from https://pypi.org/project/kivy-garden.mapview/ 
#
#pip install mapview
#from kivy_garden.mapview import MapView
#from kivy.app import App
#
#class MapViewApp(App):
#  def build(self):
#    mapview = MapView(zoom=11, lat=50.6394, lon=3.057)
#    return mapview
#
#MapViewApp().run()
#

#==========================================

echo Refs:
echo https://github.com/kivy-garden/mapview.git
echo https://pypi.org/project/kivy-garden.mapview/
echo https://kivy.org/doc/stable/guide/packaging-android.html
echo https://github.com/kivy/python-for-android
echo https://python.hotexamples.com/site/file?hash=0x1be93a046a7b9323c789451f91f0a9368981284a32de44947db3f15781628d02&fullName=kvMaps.py&project=elpaso/kivyMaps
echo https://www.geeksforgeeks.org/save-matplotlib-figure-as-svg-and-pdf-using-python/

echo TBD https://github.com/brython-dev/brython.git -- python3 runtime in the browser
echo TBD pylowtran surface to air IR sig with and without wildfire present ala https://github.com/space-physics/lowtran
