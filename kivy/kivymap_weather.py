#!/usr/bin/env python
# wget -O gville.noaa_nws.geojson 'http://forecast.weather.gov/MapClick.php?lat=29.6805&lon=-82.4271&unit=0&lg=english&FcstType=json'
import sys
from kivy.app import App
from kivy.garden.mapview import MapView, MapMarker
# need geojson markers or layer

_zoom = 11
_lat = 50.6394
_lon = 3.057
_mapurl = 'https://tile.openstreetmap.org/{z}/{x}/{y}.png'

def fetch_geojson(url='http://forecast.weather.gov/MapClick.php?lat=29.6805&lon=-82.4271&unit=0&lg=english&FcstType=json'):
  json = '[{}]'
  return json

class MapViewApp(App):
  def build(self):
    map = MapView(zoom = _zoom, lat = _lat, lon = _lon)
    m1 = MapMarker(lon=50.6394, lat=3.057)  # Lille
    map.add_marker(m1)
    m2 = MapMarker(lon=-33.867, lat=151.206)  # Sydney
    map.add_marker(m2)
    # somehow add json info fetcheed fromL
    json = fetch_geojson()
    map.add_geojson(json)
    return map

def main(args):
  """
  pip3 install kivy[full] kivy-garden 
  garden install matplotlib
  garden install grapgh
  https://realpython.com/mobile-app-kivy-python/
  https://mapview.readthedocs.io/en/latest/usage/
  https://github.com/flatplanet/kivyMD/blob/main/map.py
  """
  map = MapViewApp()
  mapsrc = MapSource(url=_mapurl,cache_key="OSMtiles", tile_size=512,image_ext="png", attribution="@ NESDIS")
  map.map_source = mapsrc
  map.run()

if __name__ == "__main__":
  args = []
  if len(sys.argv) > 1: args = copy.deepcopy(sys.argv)
  res = main(args)
  sys.exit(0)

