#!/bin/bash
echo https://github.com/space-physics -- fortran and python
echo https://github.com/space-physics/lowtran
echo https://apt.llvm.org/
echo https://zenodo.org/
echo apt install gfortran
echo apt install clang-format clang-tidy clang-tools clang clangd libc++-dev libc++1 libc++abi-dev libc++abi1 libclang-dev libclang1 liblldb-dev libllvm-ocaml-dev libomp-dev libomp5 lld lldb llvm-dev llvm-runtime llvm python3-clang
