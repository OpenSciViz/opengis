#!/bin/bash
url='https://geolocation-db.com/jsonp'
4science='69.30.253.2'
eviz='74.91.21.42'
90ghz='173.208.235.146'
curl -n --anyauth --haproxy-protocol ${url}/${4science}

#!/usr/bin/env python3
import urllib.request
import json

with urllib.request.urlopen("https://geolocation-db.com/json") as url:
data = json.loads(url.read().decode())
print(data)
