#!/usr/bin/env python
#https://adamtheautomator.com/python-wget/
import wget

url = 'time.nist.gov'
outfile = 'nist_time.txt'
wget.download(url, outfile)

esri_url = 'https://services.arcgis.com/yiPH20XcjYFQj0TD/arcgis/rest/services/NGFS_DETECTIONS_GOES-16_ABI_CONUS_2023_02_15_046/OGCFeatureServer/collections/0/items'
outfile = 'arcgis_items.json'
wget.download(esri_url, outfile)

