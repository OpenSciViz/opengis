#!/bin/bash
outpath=$(pwd)'/output'
echo "outpath == $outpath"
rm -rf $outpath >& /dev/null

function esri_test {
  outjson=$1_esri_collections.json
  if [ $1 ] ; then
    echo "esri_test arg -- $1"
    outjson=$1_esri_collections.json
    echo $outjson
  fi
  mkdir -p $outpath >& /dev/null
  ls -alqhF $outpath
  items_url='https://services.arcgis.com/yiPH20XcjYFQj0TD/arcgis/rest/services/NGFS_DETECTIONS_GOES-16_ABI_CONUS_2023_02_15_046/OGCFeatureServer/collections/0/items' 
  uagent='--user-agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36"'
  dowget="wget -d -np $uagent --auth-no-challenge --no-check-certificate -x -O $outpath/$outjson"
  echo "items_url == $items_url"
  echo "dowget == $dowget"
  echo $dowget $items_url
  $dowget $items_url
  ls -alqhF $outpath
}

### main ###
esri_test foo
esri_test bar

