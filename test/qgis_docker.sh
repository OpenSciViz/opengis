#!/bin/bash
echo docker pull qgis/qgis
echo docker network create qgis
echo docker run -d --rm --name qgis --net=qgis --hostname=qgis -v $(pwd)/data:/data:ro -p 5555:5555 -e "QGIS_PROJECT_FILE=/data/osm.qgs" qgis/qgis
