# https://support.esri.com/en/technical-article/000019701
from arcgis.gis import GIS
from arcgis.mapping import WebMap
import json

_data = {
	"operationalLayers": [{
			"id": "layer_2382",
			"layerType": "ArcGISMapServiceLayer",
			"url": "https://sampleserver6.arcgisonline.com/arcgis/rest/services/Census/MapServer",
			"visibility": True,
			"opacity": 1,
			"title": "Census Data",
			"layers": [{
					"id": 0,
					"layerDefinition": {
						"source": {
							"type": "mapLayer",
							"mapLayerId": 0
						}
					},
					"name": "Census Block Points",
					"minScale": 99999.99998945338,
					"maxScale": 0,
					"parentLayerId": -1,
					"defaultVisibility": True
				}, {
					"id": 1,
					"layerDefinition": {
						"source": {
							"type": "mapLayer",
							"mapLayerId": 1
						},
						"drawingInfo": {
							"renderer": {
								"type": "simple",
								"label": "",
								"description": "",
								"symbol": {
									"color": [0, 0, 0, 0],
									"outline": {
										"color": [230, 230, 0, 255],
										"width": 0.39975000000000005,
										"type": "esriSLS",
										"style": "esriSLSSolid"
									},
									"type": "esriSFS",
									"style": "esriSFSSolid"
								}
							}
						}
					},
					"name": "Census Block Group",
					"minScale": 1000000,
					"maxScale": 0,
					"parentLayerId": -1,
					"defaultVisibility": True
				}, {
					"id": 2,
					"layerDefinition": {
						"source": {
							"type": "mapLayer",
							"mapLayerId": 2
						},
						"drawingInfo": {
							"renderer": {
								"type": "simple",
								"label": "",
								"description": "",
								"symbol": {
									"color": [0, 0, 0, 0],
									"outline": {
										"color": [230, 230, 0, 255],
										"width": 0.5625,
										"type": "esriSLS",
										"style": "esriSLSSolid"
									},
									"type": "esriSFS",
									"style": "esriSFSSolid"
								}
							}
						}
					},
					"name": "Detailed Counties",
					"minScale": 0,
					"maxScale": 0,
					"parentLayerId": -1,
					"defaultVisibility": True
				}, {
					"id": 3,
					"layerDefinition": {
						"source": {
							"type": "mapLayer",
							"mapLayerId": 3
						},
						"drawingInfo": {
							"renderer": {
								"type": "simple",
								"label": "",
								"description": "",
								"symbol": {
									"color": [0, 0, 0, 0],
									"outline": {
										"color": [230, 230, 0, 255],
										"width": 2,
										"type": "esriSLS",
										"style": "esriSLSSolid"
									},
									"type": "esriSFS",
									"style": "esriSFSSolid"
								}
							}
						}
					},
					"name": "states",
					"minScale": 0,
					"maxScale": 0,
					"parentLayerId": -1,
					"defaultVisibility": True
				}
			]
		}
	],
	"baseMap": {
		"baseMapLayers": [{
				"id": "labels",
				"layerType": "ArcGISTiledMapServiceLayer",
				"url": "https://services.arcgisonline.com/arcgis/rest/services/Canvas/World_Dark_Gray_Base/MapServer",
				"visibility": True,
				"opacity": 1,
				"title": "World_Dark_Gray_Base"
			}, {
				"id": "base",
				"layerType": "ArcGISTiledMapServiceLayer",
				"url": "https://services.arcgisonline.com/arcgis/rest/services/Canvas/World_Dark_Gray_Reference/MapServer",
				"visibility": True,
				"opacity": 1,
				"title": "World_Dark_Gray_Reference"
			}
		],
		"title": "Basemap"
	},
	"spatialReference": {
		"wkid": 102100,
		"latestWkid": 3857
	},
	"authoringApp": "WebMapViewer",
	"authoringAppVersion": "10.6.1",
	"version": "2.11"
}

#####################################

def esri_jupyter():
  gis = GIS("https://machine.domain.com/portal","adminuser","adminpassword")
  print ("Successfully Logged Into GIS: ", gis)

  with open('/home/jupyter/test2.json') as json_data:
	jdata = json.load(json_data)

  item_properties_dict = {"type": "Web Map","title": "Test Map","tags": ["test","test1","test2"],"snippet":"This is a snippet", "text": jdata}

  newmap = gis.content.add(item_properties = item_properties_dict)
  return newmap

#################################################################

def create_map(gis, data = _data):
  if not(gis):
    gis = GIS("https://earl.esri.com/portal","siteadmin","siteadmin", verify_cert=False)
    print ("Successfully Logged Into GIS: ", gis)

  item_properties_dict = {"type": "Web Map","title": "Test Map","tags": ["test","test1","test2"],"snippet":"This is a snippet", "text":data}
  newmap = gis.content.add(item_properties = item_properties_dict)
  return newmap

  item_properties_dict = {"type": "Web Map","title": "Test Map","tags": ["test","test1","test2"],"snippet":"This is a snippet", "text":data}
  newmap = gis.content.add(item_properties = item_properties_dict)
  return newmap

